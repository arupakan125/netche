from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import View
import logging
from .forms import TopForm
import pings


class TopFormView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'index.html')

    def post(self, request, *args, **kwargs):
        target = request.POST['address']
        p = pings.Ping()
        res = p.ping(target, 1)
        result = res.to_dict()
        if result["dest_ip"] != None:
            context = {"failed": False, "ping_res": res.to_dict()}
        else:
            context = {"failed": True, "dest": target}

        return render(request, 'result.html', context)
